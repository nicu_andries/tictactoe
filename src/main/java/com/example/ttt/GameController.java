package com.example.ttt;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.shape.Line;

import java.net.URL;
import java.util.*;

public class GameController {
    public Line linie1;
    public Line linie2;
    public Line linie3;
    public Line coloana1;
    public Line coloana2;
    public Line coloana3;
    public Line diagonala03;
    public Line diagonala30;
    public Button retryButton;
    private Integer playerTurn;
    public Button Button0;
    public Button Button1;
    public Button Button2;
    public Button Button3;
    public Button Button4;
    public Integer counterX;
    public Integer counterO;
    public Button Button5;
    public Button Button6;
    public Button Button7;
    public Button Button8;
    public Label scoreLabel;
    public Label winLabel;
    public Integer finished;
    
    public void checkForWiningsX() {
        if (Button0.getText().equals("X") && Button1.getText().equals("X") && Button2.getText().equals("X")) {
            winLabel.setText("Player1 wins!");
            counterX++;
            scoreLabel.setText(counterX + " - " + counterO);
            finished = 1;
            linie1.setVisible(true);
        } else if (Button3.getText().equals("X") && Button4.getText().equals("X") && Button5.getText().equals("X")) {
            winLabel.setText("Player1 wins!");
            counterX++;
            scoreLabel.setText(counterX + " - " + counterO);
            finished = 1;
            linie2.setVisible(true);
        } else if (Button6.getText().equals("X") && Button7.getText().equals("X") && Button8.getText().equals("X")) {
            linie3.setVisible(true);
            counterX++;
            scoreLabel.setText(counterX + " - " + counterO);
            finished = 1;
            winLabel.setText("Player1 wins!");
        } else if (Button0.getText().equals("X") && Button3.getText().equals("X") && Button6.getText().equals("X")) {
            winLabel.setText("Player1 wins!");
            counterX++;
            scoreLabel.setText(counterX + " - " + counterO);
            finished = 1;
            coloana1.setVisible(true);
        } else if (Button1.getText().equals("X") && Button4.getText().equals("X") && Button7.getText().equals("X")) {
            coloana2.setVisible(true);
            counterX++;
            scoreLabel.setText(counterX + " - " + counterO);
            finished = 1;
            winLabel.setText("Player1 wins!");
        } else if (Button2.getText().equals("X") && Button5.getText().equals("X") && Button8.getText().equals("X")) {
            winLabel.setText("Player1 wins!");
            counterX++;
            scoreLabel.setText(counterX + " - " + counterO);
            finished = 1;
            coloana3.setVisible(true);
        }
        else if (Button0.getText().equals("X") && Button4.getText().equals("X") && Button8.getText().equals("X")) {
            winLabel.setText("Player1 wins!");
            counterX++;
            scoreLabel.setText(counterX + " - " + counterO);
            finished = 1;
            diagonala03.setVisible(true);
        }
        else if (Button2.getText().equals("X") && Button4.getText().equals("X") && Button6.getText().equals("X")) {
            winLabel.setText("Player1 wins!");
            counterX++;
            scoreLabel.setText(counterX + " - " + counterO);
            finished = 1;
            diagonala30.setVisible(true);
        }
    }

    public void checkForWiningsO() {
        if (Button0.getText().equals("O") && Button1.getText().equals("O") && Button2.getText().equals("O")) {
            winLabel.setText("Player2 wins!");
            counterO++;
            scoreLabel.setText(counterX + " - " + counterO);
            finished = 1;
            linie1.setVisible(true);
        } else if (Button3.getText().equals("O")&& Button4.getText().equals("O") && Button5.getText().equals("O")) {
            winLabel.setText("Player2 wins!");
            counterO++;
            scoreLabel.setText(counterX + " - " + counterO);
            finished = 1;
            linie2.setVisible(true);
        } else if (Button6.getText().equals("O") && Button7.getText().equals("O") && Button8.getText().equals("O")) {
            linie3.setVisible(true);
            counterO++;
            scoreLabel.setText(counterX + " - " + counterO);
            finished = 1;
            winLabel.setText("Player2 wins!");
        } else if (Button0.getText().equals("O") && Button3.getText().equals("O") && Button6.getText().equals("O")) {
            winLabel.setText("Player2 wins!");
            counterO++;
            scoreLabel.setText(counterX + " - " + counterO);
            finished = 1;
            coloana1.setVisible(true);
        } else if (Button1.getText().equals("O") && Button4.getText().equals("O") && Button7.getText().equals("O")) {
            coloana2.setVisible(true);
            counterO++;
            scoreLabel.setText(counterX + " - " + counterO);
            finished = 1;
            winLabel.setText("Player2 wins!");
        } else if (Button2.getText().equals("O") && Button5.getText().equals("O") && Button8.getText().equals("O")) {
            winLabel.setText("Player2 wins!");
            counterO++;
            scoreLabel.setText(counterX + " - " + counterO);
            finished = 1;
            coloana3.setVisible(true);
        }
        else if (Button0.getText().equals("O") && Button4.getText().equals("O") && Button8.getText().equals("O")) {
            winLabel.setText("Player2 wins!");
            counterO++;
            scoreLabel.setText(counterX + " - " + counterO);
            finished = 1;
            diagonala03.setVisible(true);
        }
        else if (Button2.getText().equals("O") && Button4.getText().equals("O") && Button6.getText().equals("O")) {
            winLabel.setText("Player2 wins!");
            counterO++;
            scoreLabel.setText(counterX + " - " + counterO);
            finished = 1;
            diagonala30.setVisible(true);
        }
    }

    public void refresh() {
        linie1.setVisible(false);
        linie2.setVisible(false);
        linie3.setVisible(false);
        coloana1.setVisible(false);
        coloana2.setVisible(false);
        coloana3.setVisible(false);
        diagonala03.setVisible(false);
        diagonala30.setVisible(false);
        playerTurn = 1;
        winLabel.setText("Player1's turn!");
        finished = 0;
    }

    public void initialize() {
       refresh();
       counterO = 0;
       counterX = 0;
       scoreLabel.setText("0 - 0");
    }

    public void onButton0(ActionEvent actionEvent) {
        if (playerTurn.equals(1) && finished.equals(0)) {
            if (Button0.getText().isEmpty()) {
                Button0.setText("X");
                winLabel.setText("Player2's turn!");
                playerTurn = 2;
                checkForWiningsX();
            }
        } else if (playerTurn.equals(2) && finished.equals(0)) {
            if (Button0.getText().isEmpty()) {
                Button0.setText("O");
                winLabel.setText("Player1's turn!");
                playerTurn = 1;
                checkForWiningsO();
            }
        }
    }

    public void onButton1(ActionEvent actionEvent) {
        if (playerTurn.equals(1) && finished.equals(0)) {
            if (Button1.getText().isEmpty()) {
                Button1.setText("X");
                winLabel.setText("Player2's turn!");
                checkForWiningsX();
                playerTurn = 2;
            }
        } else if (playerTurn.equals(2) && finished.equals(0)) {
            if (Button1.getText().isEmpty()) {
                Button1.setText("O");
                winLabel.setText("Player1's turn!");
                playerTurn = 1;
                checkForWiningsO();
            }
        }
    }

    public void onButton2(ActionEvent actionEvent) {
        if (playerTurn.equals(1) && finished.equals(0)) {
            if (Button2.getText().isEmpty()) {
                Button2.setText("X");
                winLabel.setText("Player2's turn!");
                playerTurn = 2;
                checkForWiningsX();
            }
        } else if (playerTurn.equals(2) && finished.equals(0)) {
            if (Button2.getText().isEmpty()) {
                Button2.setText("O");
                winLabel.setText("Player1's turn!");
                playerTurn = 1;
                checkForWiningsO();
            }
        }
    }

    public void onButton3(ActionEvent actionEvent) {
        if (playerTurn.equals(1) && finished.equals(0)) {
            if (Button3.getText().isEmpty()) {
                Button3.setText("X");
                winLabel.setText("Player2's turn!");
                playerTurn = 2;
                checkForWiningsX();
            }
        } else if (playerTurn.equals(2) && finished.equals(0)) {
            if (Button3.getText().isEmpty()) {
                Button3.setText("O");
                winLabel.setText("Player1's turn!");
                playerTurn = 1;
                checkForWiningsO();
            }
        }
    }

    public void onButton4(ActionEvent actionEvent) {
        if (playerTurn.equals(1) && finished.equals(0)) {
            if (Button4.getText().isEmpty()) {
                Button4.setText("X");
                winLabel.setText("Player2's turn!");
                playerTurn = 2;
                checkForWiningsX();
            }
        } else if (playerTurn.equals(2) && finished.equals(0)) {
            if (Button4.getText().isEmpty()) {
                Button4.setText("O");
                winLabel.setText("Player1's turn!");
                playerTurn = 1;
                checkForWiningsO();
            }
        }
    }

    public void onButton5(ActionEvent actionEvent) {
        if (playerTurn.equals(1) && finished.equals(0)) {
            if (Button5.getText().isEmpty()) {
                Button5.setText("X");
                winLabel.setText("Player2's turn!");
                playerTurn = 2;
                checkForWiningsX();
            }
        } else if (playerTurn.equals(2) && finished.equals(0)) {
            if (Button5.getText().isEmpty()) {
                Button5.setText("O");
                winLabel.setText("Player1's turn!");
                playerTurn = 1;
                checkForWiningsO();
            }
        }
    }

    public void onButton6(ActionEvent actionEvent) {
        if (playerTurn.equals(1) && finished.equals(0)) {
            if (Button6.getText().isEmpty()) {
                Button6.setText("X");
                winLabel.setText("Player2's turn!");
                playerTurn = 2;
                checkForWiningsX();
            }
        } else if (playerTurn.equals(2) && finished.equals(0)) {
            if (Button6.getText().isEmpty()) {
                Button6.setText("O");
                winLabel.setText("Player1's turn!");
                playerTurn = 1;
                checkForWiningsO();
            }
        }
    }

    public void onButton7(ActionEvent actionEvent) {
        if (playerTurn.equals(1) && finished.equals(0)) {
            if (Button7.getText().isEmpty()) {
                Button7.setText("X");
                winLabel.setText("Player2's turn!");
                playerTurn = 2;
                checkForWiningsX();
            }
        } else if (playerTurn.equals(2) && finished.equals(0)) {
            if (Button7.getText().isEmpty()) {
                Button7.setText("O");
                winLabel.setText("Player1's turn!");
                playerTurn = 1;
                checkForWiningsO();
            }
        }
    }

    public void onButton8(ActionEvent actionEvent) {
        if (playerTurn.equals(1) && finished.equals(0)) {
            if (Button8.getText().isEmpty()) {
                Button8.setText("X");
                winLabel.setText("Player2's turn!");
                playerTurn = 2;
                checkForWiningsX();
            }
        } else if (playerTurn.equals(2) && finished.equals(0)) {
            if (Button8.getText().isEmpty()) {
                Button8.setText("O");
                winLabel.setText("Player1's turn!");
                playerTurn = 1;
                checkForWiningsO();
            }
        }
    }

    public void retryButton(ActionEvent actionEvent) {
        refresh();
        Button0.setText("");
        Button1.setText("");
        Button2.setText("");
        Button3.setText("");
        Button4.setText("");
        Button5.setText("");
        Button6.setText("");
        Button7.setText("");
        Button8.setText("");
    }
}